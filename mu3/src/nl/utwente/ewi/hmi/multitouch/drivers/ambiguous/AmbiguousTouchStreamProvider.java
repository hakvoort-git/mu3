package nl.utwente.ewi.hmi.multitouch.drivers.ambiguous;

import java.awt.geom.AffineTransform;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import nl.utwente.ewi.hmi.multitouch.TouchDevice;
import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;
import nl.utwente.ewi.hmi.multitouch.io.TouchStream;
import nl.utwente.ewi.hmi.multitouch.io.TouchStreamProvider;

public class AmbiguousTouchStreamProvider implements TouchStreamProvider {

	private TouchStreamProvider wrappedTouchStreamProvider = null;
	
	private List<TouchDeviceInfo> ambiguousTouchDevices = null;
	

	private Map<TouchDeviceInfo, TouchStream> ambiguousTouchStreams = null;
	
	public AmbiguousTouchStreamProvider(TouchStreamProvider wrappedTouchStreamProvider) {
		this.wrappedTouchStreamProvider = wrappedTouchStreamProvider;
		this.ambiguousTouchStreams = new HashMap<TouchDeviceInfo, TouchStream>();
	}

	@Override
	public List<TouchDeviceInfo> getTouchDeviceInfoList() {
		if(this.ambiguousTouchDevices == null) {
			this.ambiguousTouchDevices = new LinkedList<TouchDeviceInfo>();

			for(TouchDeviceInfo wrappedTouchDeviceInfo : wrappedTouchStreamProvider.getTouchDeviceInfoList()) {
				this.ambiguousTouchDevices.add(new AmbiguousTouchDeviceInfo(wrappedTouchDeviceInfo));
			}
		}
		return this.ambiguousTouchDevices;
	}

	@Override
	public TouchStream getTouchStream(TouchDeviceInfo deviceInfo) {
		if(!(deviceInfo instanceof AmbiguousTouchDeviceInfo)) {
			return null;
		}

		AmbiguousTouchDeviceInfo ambiguousTouchDeviceInfo = (AmbiguousTouchDeviceInfo) deviceInfo;
		
		TouchDeviceInfo wrappedTouchDeviceInfo = ambiguousTouchDeviceInfo.getWrappedTouchDeviceInfo();
		
		if(this.ambiguousTouchStreams.containsKey(deviceInfo)) {
			return this.ambiguousTouchStreams.get(deviceInfo);
		}

		TouchStream touchStream = wrappedTouchStreamProvider.getTouchStream(wrappedTouchDeviceInfo);
		
		TouchStream ambiguousTouchStream = new AmbiguousTouchStream(touchStream, ambiguousTouchDeviceInfo);
		
		this.ambiguousTouchStreams.put(ambiguousTouchDeviceInfo, ambiguousTouchStream);

		return ambiguousTouchStream;
	}

}
