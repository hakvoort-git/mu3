package nl.utwente.ewi.hmi.multitouch.drivers.calibrate;

import java.io.IOException;
import java.util.Set;

import nl.utwente.ewi.hmi.multitouch.analysis.Segment;
import nl.utwente.ewi.hmi.multitouch.io.TouchStream;

public class CalibratedTouchStream extends TouchStream {

	private TouchStream wrappedTouchStream = null;
	
	public CalibratedTouchStream(TouchStream wrappedTouchStream, CalibratedTouchDeviceInfo touchDeviceInfo) {
		super(touchDeviceInfo);
		this.wrappedTouchStream = wrappedTouchStream;
	}

	@Override
	public boolean close() throws IOException {
		return this.wrappedTouchStream.close();
	}

	@Override
	public Set<Object> getActors() {
		return this.wrappedTouchStream.getActors();
	}

	
	@Override
	public boolean isOpen() {
		return this.wrappedTouchStream.isOpen();
	}

	@Override
	public boolean open() throws IOException {
		return this.wrappedTouchStream.open();
	}

	@Override
	public void read(Set<Segment> segments) throws IOException {
		this.wrappedTouchStream.read(segments);
	}

}
