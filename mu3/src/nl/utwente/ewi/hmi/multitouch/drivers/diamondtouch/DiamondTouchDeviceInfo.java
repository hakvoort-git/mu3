package nl.utwente.ewi.hmi.multitouch.drivers.diamondtouch;

import java.awt.Dimension;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

/**
 * The DiamondTouchDeviceInfo provides additional information about a Diamond Touch device.
 * 
 * @author Michiel Hakvoort
 * @version 1.0
 */
public class DiamondTouchDeviceInfo extends TouchDeviceInfo {

	private int userCount = 0;

	/**
	 * 
	 * @param userCount The maximum amount of users which can concurrently use the table
	 * @param size The dimensions of the table
	 * @param deviceID The id of the device
	 */
	public DiamondTouchDeviceInfo(int userCount, Dimension size, String deviceID) {
		super(size, deviceID, true);

		this.userCount = userCount;
	}

	/**
	 * Retrieve the maximum amount of users which can concurrently use the table.
	 * 
	 * @return The maximum amount of users which can concurrently use the table
	 */
	public int getUserCount() {
		return this.userCount;
	}
}
