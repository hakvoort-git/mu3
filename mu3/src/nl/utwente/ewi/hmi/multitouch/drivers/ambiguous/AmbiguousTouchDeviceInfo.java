package nl.utwente.ewi.hmi.multitouch.drivers.ambiguous;

import java.awt.Dimension;
import java.awt.geom.AffineTransform;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public class AmbiguousTouchDeviceInfo extends TouchDeviceInfo {

	private TouchDeviceInfo wrappedTouchDeviceInfo;

	public AmbiguousTouchDeviceInfo(TouchDeviceInfo wrappedTouchDeviceInfo) {
		super(wrappedTouchDeviceInfo.getSize(), wrappedTouchDeviceInfo.getDeviceID(), true);
		this.wrappedTouchDeviceInfo = wrappedTouchDeviceInfo;
	}

	public TouchDeviceInfo getWrappedTouchDeviceInfo() {
		return this.wrappedTouchDeviceInfo;
	}

	@Override
	public AffineTransform getTransform() {
		return wrappedTouchDeviceInfo.getTransform();
	}

	@Override
	public Dimension getTransformedSize() {
		return wrappedTouchDeviceInfo.getTransformedSize();
	}
}
