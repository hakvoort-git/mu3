package nl.utwente.ewi.hmi.multitouch.drivers.dtproxy;

import java.util.List;

public class DiamondTouchFrame {

	public final long timeStamp;
	public final int columns;
	public final int rows;
	public final List<DiamondTouchSegment> xSegments;
	public final List<DiamondTouchSegment> ySegments;

	public DiamondTouchFrame(long timeStamp, int columns, int rows, List<DiamondTouchSegment> xSegments, List<DiamondTouchSegment> ySegments) {
		this.timeStamp = timeStamp;
		this.columns = columns;
		this.rows = rows;
		this.xSegments = xSegments;
		this.ySegments = ySegments;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append('@');
		builder.append(timeStamp);
		builder.append(":[");
		builder.append(columns);
		builder.append('x');
		builder.append(rows);
		builder.append(" ");
		builder.append(xSegments);
		builder.append(ySegments);
		
		return builder.toString();
	}
}
