package nl.utwente.ewi.hmi.multitouch.drivers.tuio;

import java.awt.Dimension;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public class TuioTouchDeviceInfo extends TouchDeviceInfo {

	private int port;

	public TuioTouchDeviceInfo(int port) {
		super(new Dimension(640,480), "TUIO:" + port);
		this.port = port;
	}

	public int getPort() {
		return this.port;
	}
	
}
