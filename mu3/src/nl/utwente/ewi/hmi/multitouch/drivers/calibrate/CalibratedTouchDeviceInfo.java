package nl.utwente.ewi.hmi.multitouch.drivers.calibrate;

import java.awt.geom.AffineTransform;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public class CalibratedTouchDeviceInfo extends TouchDeviceInfo {

	private TouchDeviceInfo wrappedTouchDeviceInfo;
	private AffineTransform transform;
	
	public CalibratedTouchDeviceInfo(TouchDeviceInfo wrappedTouchDeviceInfo) {
		super(wrappedTouchDeviceInfo.getSize(), wrappedTouchDeviceInfo.getDeviceID(), wrappedTouchDeviceInfo.isAmbiguous());
		this.wrappedTouchDeviceInfo = wrappedTouchDeviceInfo;
	}

	public TouchDeviceInfo getWrappedTouchDeviceInfo() {
		return this.wrappedTouchDeviceInfo;
	}

	public AffineTransform getTransform() {
		return this.transform;
	}

	public void setTransform(AffineTransform transform) {
		this.transform = transform;
	}
}
