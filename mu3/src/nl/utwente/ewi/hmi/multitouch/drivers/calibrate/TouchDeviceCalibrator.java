package nl.utwente.ewi.hmi.multitouch.drivers.calibrate;

import java.awt.geom.AffineTransform;

import nl.utwente.ewi.hmi.multitouch.TouchDevice;

public interface TouchDeviceCalibrator {

	public AffineTransform calibrate(TouchDevice device);

}
