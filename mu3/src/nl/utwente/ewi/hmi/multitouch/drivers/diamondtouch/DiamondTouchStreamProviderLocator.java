package nl.utwente.ewi.hmi.multitouch.drivers.diamondtouch;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Pattern;

import nl.utwente.ewi.hmi.multitouch.io.TouchStreamProvider;
import nl.utwente.ewi.hmi.multitouch.io.TouchStreamProviderLocator;

public class DiamondTouchStreamProviderLocator extends TouchStreamProviderLocator {

	protected static final String SCHEME_DIAMONDTOUCH = "diamondtouch";
	protected static final String SCHEME_ANY = "any";
	protected static final Pattern DEVICE_PATTERN = Pattern.compile("^\\d+$");

	@Override
	public TouchStreamProvider locateTouchStreamProvider(URI uri) {
		
		// return a diamondtouchstreamprovider
		return null;
	}

	@Override
	public boolean validTouchStreamProvider(URI uri) {
		String scheme = uri.getScheme();

		if(!SCHEME_IO.equals(scheme)) {
			return false;
		}
		
		try {
			uri = new URI(uri.getSchemeSpecificPart());
		} catch (URISyntaxException e) {
			return false;
		}

		scheme = uri.getScheme();
		
		if(!SCHEME_TOUCH.equals(scheme)) {
			return false;
		}
		
		try {
			uri = new URI(uri.getSchemeSpecificPart());
		} catch (URISyntaxException e) {
			return false;
		}
		
		scheme = uri.getScheme();
		
		if(!SCHEME_DIAMONDTOUCH.equals(scheme)) {
			return false;
		}
		
		if(SCHEME_ANY.equals(uri.getSchemeSpecificPart())) {
			return true;
		}

		try {
			uri = new URI(uri.getSchemeSpecificPart());
		} catch (URISyntaxException e) {
			return false;
		}

		return DEVICE_PATTERN.matcher(uri.getSchemeSpecificPart()).matches();
	}
	
	public static void main(String[] args) throws Exception {
		DiamondTouchStreamProviderLocator p = new DiamondTouchStreamProviderLocator();
		System.out.println(p.validTouchStreamProvider(new URI(
				"io:touch:diamondtouch:01FE-303B-921C")));;
	}

}
