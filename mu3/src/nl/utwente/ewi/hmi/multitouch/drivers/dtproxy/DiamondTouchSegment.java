package nl.utwente.ewi.hmi.multitouch.drivers.dtproxy;

public class DiamondTouchSegment {

	public final int startPos;
	public final int maxPos;
	public final int endPos;

	public DiamondTouchSegment(int startPos, int maxPos, int endPos) {
		this.startPos = startPos;
		this.maxPos = maxPos;
		this.endPos = endPos;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append('(');
		builder.append(startPos);
		builder.append('-');
		builder.append(maxPos);
		builder.append('-');
		builder.append(endPos);
		builder.append(')');
		
		return builder.toString();
	}
}
