package nl.utwente.ewi.hmi.multitouch.drivers.ambiguous;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;

import nl.utwente.ewi.hmi.multitouch.SimpleTangible;
import nl.utwente.ewi.hmi.multitouch.SurfaceType;
import nl.utwente.ewi.hmi.multitouch.Tangible;
import nl.utwente.ewi.hmi.multitouch.analysis.Segment;
import nl.utwente.ewi.hmi.multitouch.io.TouchStream;

public class AmbiguousTouchStream extends TouchStream {

	private TouchStream wrappedTouchStream = null;

	private Tangible tangible = null;

	private Frame[] frameResult = null; 
	private static final Frame[] NO_FRAMES = new Frame[0];

	private boolean[] xs = null;
	private boolean[] ys = null;

	private TouchType[] touchTypeMap = null;
	private Tangible[] tangibleMap = null;

	public AmbiguousTouchStream(TouchStream wrappedTouchStream, AmbiguousTouchDeviceInfo touchDeviceInfo) {
		super(touchDeviceInfo);

		this.wrappedTouchStream = wrappedTouchStream;

		xs = new boolean[(int) touchDeviceInfo.getSize().getWidth()];
		ys = new boolean[(int) touchDeviceInfo.getSize().getHeight()];

		tangible = new SimpleTangible(new Object(), SurfaceType.UNKNOWN);
		this.frameResult = new Frame[] { new Frame(touchDeviceInfo.getSize()) };

		this.touchTypeMap = new TouchType[xs.length * ys.length];
		this.tangibleMap = new Tangible[xs.length * ys.length];
	}

	@Override
	public boolean close() throws IOException {
		return this.wrappedTouchStream.close();
	}

	@Override
	public Set<Object> getActors() {
		return this.wrappedTouchStream.getActors();
	}

	/*
	@Override
	public Frame[] getFrames() throws IOException {
		Frame[] frames = this.wrappedTouchStream.getFrames();

		if(frames.length == 0) {
			return NO_FRAMES;
		}

		Arrays.fill(xs, false);
		Arrays.fill(ys, false);
		Arrays.fill(tangibleMap, null);
		Arrays.fill(touchTypeMap, TouchType.NEGATIVE);
		

		Frame targetFrame = this.frameResult[0];

		int width = this.touchDeviceInfo.getSize().width;
		
		for(Frame frame : frames) {
			TouchType[] touchTypes = frame.getTouchTypeMap();
			
			for(int i = 0; i < touchTypes.length; i++) {
				if(touchTypes[i] == TouchType.NEGATIVE || touchTypes[i] == TouchType.NEVER) {
					continue;
				}
				int x = i % width;
				int y = i / width;
				
				xs[x] = true;
				ys[y] = true;
			}
		}

		for(int x = 0; x < xs.length; x++) {
			if(!xs[x]) { continue; }
			for(int y = 0; y < ys.length; y++) {
				if(!ys[y]) { continue; }
				int i = y * width + x;
				touchTypeMap[i] = TouchType.UNCERTAIN;
				tangibleMap[i] = tangible;
			}
		}
		
		targetFrame.writeTangibleMap(tangibleMap);
		targetFrame.writeTouchTypeMap(touchTypeMap);

		return this.frameResult;
	}
	*/

	@Override
	public boolean open() throws IOException {
		return this.wrappedTouchStream.open();
	}

	@Override
	public boolean isOpen() {
		return this.wrappedTouchStream.isOpen();
	}

	@Override
	public void read(Set<Segment> segments) throws IOException {
		// TODO Auto-generated method stub
		
	}

}
