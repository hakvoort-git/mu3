package nl.utwente.ewi.hmi.multitouch.drivers.tuio;

import nl.utwente.ewi.hmi.multitouch.SimpleTangible;
import nl.utwente.ewi.hmi.multitouch.SurfaceType;

public class TuioTangible extends SimpleTangible {

	private Long sessionId;

	public TuioTangible(Object actor, long sessionId) {
		super(actor, SurfaceType.UNKNOWN);

		this.sessionId = sessionId;
	}

	@Override
	public int hashCode() {
		return this.sessionId.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null || !(obj instanceof TuioTangible)) {
			return false;
		}
		
		TuioTangible other = (TuioTangible) obj;

		return other.sessionId.equals(this.sessionId);
	}
	
	@Override
	public boolean isUnique() {
		return true;
	}
}
