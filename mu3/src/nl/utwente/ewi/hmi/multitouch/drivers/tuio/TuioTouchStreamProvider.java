package nl.utwente.ewi.hmi.multitouch.drivers.tuio;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;
import nl.utwente.ewi.hmi.multitouch.io.TouchStream;
import nl.utwente.ewi.hmi.multitouch.io.TouchStreamProvider;

public class TuioTouchStreamProvider implements TouchStreamProvider {

	private static final int TUIO_DEFAULT_PORT = 3333;

	private TuioTouchDeviceInfo touchDeviceInfo;
	
	private List<TouchDeviceInfo> deviceInfos = null;
	
	private TuioTouchStream stream = null;
	
	public TuioTouchStreamProvider(int port) {
		this.touchDeviceInfo = new TuioTouchDeviceInfo(port);
		this.deviceInfos = Collections.unmodifiableList(Arrays.asList(new TouchDeviceInfo[] { this.touchDeviceInfo }));
		this.stream = new TuioTouchStream(touchDeviceInfo);
	}

	public TuioTouchStreamProvider() {
		this(TUIO_DEFAULT_PORT);
	}

	@Override
	public List<TouchDeviceInfo> getTouchDeviceInfoList() {
		return this.deviceInfos;
	}

	@Override
	public TouchStream getTouchStream(TouchDeviceInfo deviceInfo) {
		if(deviceInfo != touchDeviceInfo) {
			return null;
		}

		return this.stream;
		
	}

}
