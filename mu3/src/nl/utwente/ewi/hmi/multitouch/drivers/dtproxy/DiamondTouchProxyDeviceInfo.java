package nl.utwente.ewi.hmi.multitouch.drivers.dtproxy;

import java.awt.Dimension;
import java.net.InetAddress;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public class DiamondTouchProxyDeviceInfo extends TouchDeviceInfo {

	private final int port;
	private final InetAddress host;
	private final int userCount;

	public DiamondTouchProxyDeviceInfo(Dimension dimension, int userCount, InetAddress host, int port) {
		super(dimension, "DTProxy " + host + ":" + port, true);

		this.userCount = userCount;
		this.port = port;
		this.host = host;
	}

	public int getPort() {
		return this.port;
	}

	public InetAddress getHost() {
		return this.host;
	}

	public int getUserCount() {
		return this.userCount;
	}
}
