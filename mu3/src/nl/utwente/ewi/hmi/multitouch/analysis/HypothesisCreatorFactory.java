package nl.utwente.ewi.hmi.multitouch.analysis;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public interface HypothesisCreatorFactory {

	public HypothesisCreator getHypothesisCreator(TouchDeviceInfo deviceInfo);
}
