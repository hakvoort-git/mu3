package nl.utwente.ewi.hmi.multitouch.analysis.segmentation;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public class ContourTracingSegmentationFactory implements FrameSegmentationFactory {

	public FrameSegmentation getFrameSegmentation(TouchDeviceInfo touchDeviceInfo) {
		return new ContourTracingSegmentation(touchDeviceInfo);
	}
}
