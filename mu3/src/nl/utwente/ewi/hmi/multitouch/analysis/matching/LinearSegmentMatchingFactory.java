package nl.utwente.ewi.hmi.multitouch.analysis.matching;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public class LinearSegmentMatchingFactory implements SegmentMatchingFactory {

	@Override
	public SegmentMatching getSegmentMatching(TouchDeviceInfo touchDeviceInfo) {
		return new LinearSegmentMatching(touchDeviceInfo);
	}

}
