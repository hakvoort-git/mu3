package nl.utwente.ewi.hmi.multitouch.analysis.segmentation;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public class ComponentFillSegmentationFactory implements FrameSegmentationFactory {

	public FrameSegmentation getFrameSegmentation(TouchDeviceInfo touchDeviceInfo) {
		return new ComponentFillSegmentation(touchDeviceInfo);
	}
}
