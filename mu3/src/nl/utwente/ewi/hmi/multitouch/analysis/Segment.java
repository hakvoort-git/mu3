package nl.utwente.ewi.hmi.multitouch.analysis;

import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import nl.utwente.ewi.hmi.multitouch.Tangible;

public interface Segment extends Comparable<Segment> {

	public Tangible getTangible();

	public float getMass();

	public Path2D getPerimeter();
	public Point2D getOrigin();
	public Rectangle2D getBounds();
	
	public boolean isAmbiguous();
	
	/**
	 * Return the time at which the segment was registered
	 * 
	 * @return
	 */
	public long getTime();
	
}
