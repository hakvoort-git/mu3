package nl.utwente.ewi.hmi.multitouch.analysis.segmentation;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public interface FrameSegmentationFactory {

	public FrameSegmentation getFrameSegmentation(TouchDeviceInfo touchDeviceInfo);
}
