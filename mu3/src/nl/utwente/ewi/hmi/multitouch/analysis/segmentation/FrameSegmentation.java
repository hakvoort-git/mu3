package nl.utwente.ewi.hmi.multitouch.analysis.segmentation;

import java.util.Set;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;
import nl.utwente.ewi.hmi.multitouch.analysis.Segment;
import nl.utwente.ewi.hmi.multitouch.io.TouchStream.FrameBuffer;

public abstract class FrameSegmentation {

	protected TouchDeviceInfo touchDeviceInfo = null;
	
	public FrameSegmentation(TouchDeviceInfo touchDeviceInfo) {
		this.touchDeviceInfo = touchDeviceInfo;
	}

	public TouchDeviceInfo getTouchDeviceInfo() {
		return this.touchDeviceInfo;
	}

	// force per frame segmentation
	public abstract Set<Segment> segment(FrameBuffer buffer);
}
