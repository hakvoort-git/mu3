package nl.utwente.ewi.hmi.multitouch.analysis.matching;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public interface SegmentMatchingFactory {

	public SegmentMatching getSegmentMatching(TouchDeviceInfo touchDeviceInfo);
}
