package nl.utwente.ewi.hmi.multitouch.analysis;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public class GreedyHypothesisCreatorFactory implements HypothesisCreatorFactory {

	@Override
	public HypothesisCreator getHypothesisCreator(TouchDeviceInfo deviceInfo) {
		return new GreedyHypothesisCreator(deviceInfo);
	}

}
