package nl.utwente.ewi.hmi.multitouch.analysis;

import java.util.Set;

public interface HypothesisFilter {

	public Set<Hypothesis> filter(Set<Hypothesis> hypotheses);
}
