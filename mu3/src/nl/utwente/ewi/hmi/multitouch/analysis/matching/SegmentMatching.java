package nl.utwente.ewi.hmi.multitouch.analysis.matching;

import java.util.NavigableSet;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;
import nl.utwente.ewi.hmi.multitouch.analysis.Segment;

public abstract class SegmentMatching {

	protected TouchDeviceInfo touchDeviceInfo = null;
	
	public SegmentMatching(TouchDeviceInfo touchDeviceInfo) {
		this.touchDeviceInfo = touchDeviceInfo;
	}
	
	public abstract double getConfidence(NavigableSet<Segment> history, Segment testSegment);

}
