package nl.utwente.ewi.hmi.multitouch.analysis;

public class Hypothesis {

	private Segment fromSegment = null;
	private Segment toSegment = null;

	public Hypothesis(Segment fromSegment, Segment toSegment) {
		this.fromSegment = fromSegment;
		this.toSegment = toSegment;
	}

	public Segment getFromSegment() {
		return this.fromSegment;
	}

	public Segment getToSegment() {
		return this.toSegment;
	}

	public boolean isEndOfTrajectory() {
		return this.fromSegment != null && this.toSegment == null;
	}

	public boolean isStartOfTrajectory() {
		return this.fromSegment == null && this.toSegment != null;
	}
}
