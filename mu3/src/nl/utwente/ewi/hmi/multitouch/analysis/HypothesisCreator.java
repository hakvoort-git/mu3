package nl.utwente.ewi.hmi.multitouch.analysis;

import java.util.Set;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;

public abstract class HypothesisCreator {

	protected TouchDeviceInfo deviceInfo = null;
	
	public HypothesisCreator(TouchDeviceInfo deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public abstract Set<Hypothesis> getHypotheses(Set<Segment> newState);
}
