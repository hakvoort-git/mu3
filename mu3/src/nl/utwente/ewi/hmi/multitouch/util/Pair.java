package nl.utwente.ewi.hmi.multitouch.util;

/**
 * A simple pair holder.
 * 
 * @author Michiel
 *
 * @param <A> The first type of the Pair
 * @param <B> The second type of the Pair
 */
public class Pair<A, B> {

	private A first = null;
	private B second = null;
	
	public Pair(A first, B second) {
		this.first = first;
		this.second = second;
	}

	public A getFirst() {
		return first;
	}

	public B getSecond() {
		return second;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		} else if(obj == this) {
			return true;
		}

		@SuppressWarnings("unchecked")
		Pair<A, B> other = (Pair<A,B>) obj;
		
		return this.first.equals(other.first) && this.second.equals(other.second);
	}

	@Override
	public int hashCode() {
		return (int)(((long)first.hashCode()) + ((long)second.hashCode()) / 2);
	}

	public static <A, B, C> Pair<A, Pair<B, C>> wrap(A first, B second, C third) {
		return new Pair<A, Pair<B, C>>(first, new Pair<B, C>(second, third));
	}

	public static <A, B, C, D> Pair<A, Pair<B, Pair<C, D>>> wrap(A first, B second, C third, D fourth) {
		return new Pair<A, Pair<B, Pair<C, D>>>(first, wrap(second, third, fourth));
	}
}
