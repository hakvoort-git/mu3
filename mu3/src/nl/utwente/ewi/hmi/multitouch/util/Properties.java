package nl.utwente.ewi.hmi.multitouch.util;

import nl.utwente.ewi.hmi.multitouch.analysis.GreedyHypothesisCreatorFactory;
import nl.utwente.ewi.hmi.multitouch.analysis.HypothesisCreatorFactory;
import nl.utwente.ewi.hmi.multitouch.analysis.matching.LinearSegmentMatchingFactory;
import nl.utwente.ewi.hmi.multitouch.analysis.matching.SegmentMatchingFactory;
import nl.utwente.ewi.hmi.multitouch.analysis.segmentation.ComponentFillSegmentationFactory;
import nl.utwente.ewi.hmi.multitouch.analysis.segmentation.FrameSegmentationFactory;

public class Properties {

	public static final FrameSegmentationFactory SEGMENTATION_FACTORY;
	public static final SegmentMatchingFactory MATCHING_FACTORY;
	public static final HypothesisCreatorFactory HYPOTHESIS_FACTORY;

	static {
		SEGMENTATION_FACTORY = new ComponentFillSegmentationFactory();
		MATCHING_FACTORY = new LinearSegmentMatchingFactory();
		HYPOTHESIS_FACTORY= new GreedyHypothesisCreatorFactory();
	}
}
