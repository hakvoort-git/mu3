package nl.utwente.ewi.hmi.multitouch;

/**
 * The listener interface for the {@link TouchDevice}. 
 * 
 * @author Michiel Hakvoort
 * @version 1.0
 */
public interface TouchDeviceListener {

	/**
	 * A {@linkplain TouchDevice} is started.
	 * 
	 * @param touchDevice The {@linkplain TouchDevice} that was started
	 */
	public void touchDeviceStarted(TouchDevice touchDevice);

	/**
	 * A {@linkplain TouchDevice} is stopped.
	 * 
	 * @param touchDevice The {@linkplain TouchDevice} that was stopped
	 */
	public void touchDeviceStopped(TouchDevice touchDevice);

	/**
	 * A {@linkplain Touch} that is registered.
	 * 
	 * @param touchDevice The {@linkplain TouchDevice} for which a {@linkplain Touch} is registered
	 * @param touch The {@linkplain Touch} that was registered
	 */
	public void touchRegistered(TouchDevice touchDevice, Touch touch);
}
