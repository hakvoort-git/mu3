package nl.utwente.ewi.hmi.multitouch;

import java.awt.Dimension;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

/**
 * The TouchDeviceInfo class specifies information about a {@linkplain TouchDevice}. Each TouchDeviceInfo
 * should identify one single specific {@linkplain TouchDeviceStream}. The information the TouchDeviceInfo carries
 * is used for optimizing identification and tracking of {@linkplain Touch Touches}.
 * 
 * @author Michiel Hakvoort
 * @version 1.0
 *
 */
public class TouchDeviceInfo {

	protected Dimension size = null;
	protected String deviceID = null;
	protected boolean isAmbiguous = true;

	private static final AffineTransform IDENTITY_TRANSFORM = new AffineTransform();
	
	public TouchDeviceInfo(Dimension size, String deviceID, boolean isAmbiguous) {
		this.size = size;
		this.deviceID = deviceID;
		this.isAmbiguous = isAmbiguous;
	}

	public TouchDeviceInfo(Dimension size, String deviceID) {
		this(size, deviceID, false);
	}

	/**
	 * Get the device id of the described touch device.
	 * 
	 * @return The device id of the described touch devices
	 */
	public String getDeviceID() {
		return this.deviceID;
	}

	/**
	 * Get the dimension of the touch input.
	 * 
	 * @return The dimension of the touch input.
	 */
	public Dimension getSize() {
		return (Dimension)this.size.clone();
	}

	/**
	 * Check whether a touch device is ambiguous. Ambiguous touch devices are devices which can not identify
	 * touches at their exact locations. E.g. mutual capacitance touch devices are unable to identify touches at
	 * their exact locations, as only an array of x and an array of y signals is produced.
	 * 
	 * @return True when the touch device is ambiguous
	 */
	public boolean isAmbiguous() {
		return this.isAmbiguous;
	}

	/**
	 * Return the affine transformation which is applied to touch events. 
	 * 
	 * @return The affine transformation which is applied to touch events.
	 */
	public AffineTransform getTransform() {
		return IDENTITY_TRANSFORM;
	}

	/**
	 * Return the dimension of the transformed touch input.
	 * 
	 * @return The dimension of the transformed touch input.
	 */
	public Dimension getTransformedSize() {
		Point2D point = new Point2D.Float(getSize().width, getSize().height);
		getTransform().transform(point, point);
		return new Dimension((int)point.getX(), (int)point.getY());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return this.deviceID;
	}

	public Resolution getResolution() {
		return null;
	}
}
