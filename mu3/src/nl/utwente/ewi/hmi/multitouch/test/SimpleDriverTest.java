package nl.utwente.ewi.hmi.multitouch.test;

import java.net.InetAddress;
import java.util.List;

import nl.utwente.ewi.hmi.multitouch.Touch;
import nl.utwente.ewi.hmi.multitouch.TouchDevice;
import nl.utwente.ewi.hmi.multitouch.TouchDeviceAdapter;
import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;
import nl.utwente.ewi.hmi.multitouch.TouchEnvironment;
import nl.utwente.ewi.hmi.multitouch.drivers.dtproxy.DTProxyStreamProvider;
import nl.utwente.ewi.hmi.multitouch.event.TouchAdapter;
import nl.utwente.ewi.hmi.multitouch.event.TouchEvent;
import nl.utwente.ewi.hmi.multitouch.event.TouchJoinEvent;
import nl.utwente.ewi.hmi.multitouch.event.TouchSplitEvent;
import nl.utwente.ewi.hmi.multitouch.io.TouchStreamProvider;

public class SimpleDriverTest {

	public static void main(String[] args) throws Exception {
//		TouchStreamProvider provider = DiamondTouchStreamProvider.getDiamondTouchStreamProvider(); //(); //MouseTouchStreamProvider.getMouseTouchStreamProvider();

		TouchStreamProvider provider = new DTProxyStreamProvider(InetAddress.getLocalHost(), 10002);

//		provider = new AmbiguousTouchStreamProvider(provider);
		TouchEnvironment environment = TouchEnvironment.getTouchEnvironment();

		System.out.println("init test");
		
		environment.registerTouchStreamProvider(provider);
		
		List<TouchDeviceInfo> deviceInfos = environment.getTouchDeviceInfoList();
		if(deviceInfos.isEmpty()) {
			System.exit(1);
		}
		
		System.out.println(deviceInfos);
		
		TouchDeviceInfo deviceInfo = deviceInfos.get(0);
		TouchDevice device = environment.getTouchDevice(deviceInfo);

//		device.setThreaded(false);

		device.start();
		device.addTouchDeviceListener(new TouchDeviceAdapter() {

			@Override
			public void touchRegistered(TouchDevice touchDevice, Touch touch) {
				System.out.println("new@"+touch.getOrigin());
				touch.addTouchListener(new TouchAdapter() {
					@Override
					public void touchMoved(TouchEvent event) {
					}
					
					@Override
					public void touchReleased(TouchEvent event) {
						System.out.println("released@"+event.getSource().getOrigin());
					}
					
					@Override
					public void touchSplit(TouchSplitEvent event) {
						System.out.println("split!");
					}
					
					@Override
					public void touchJoined(TouchJoinEvent event) {
						System.out.println("join!");
					}
				});
			}
			
		});
		
		while("a".equals("a")) {
//			device.getTouchFrameProcessor().process();
			Thread.sleep(10);
		}
		
		Object obj = new Object();
		
		synchronized(obj) {
			obj.wait();
		}
	}
}
