package nl.utwente.ewi.hmi.multitouch.test;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import nl.utwente.ewi.hmi.multitouch.Touch;
import nl.utwente.ewi.hmi.multitouch.TouchDevice;
import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;
import nl.utwente.ewi.hmi.multitouch.TouchDeviceListener;
import nl.utwente.ewi.hmi.multitouch.TouchEnvironment;
import nl.utwente.ewi.hmi.multitouch.drivers.calibrate.CalibratedTouchStreamProvider;
import nl.utwente.ewi.hmi.multitouch.drivers.calibrate.FixedTransformCalibrator;
import nl.utwente.ewi.hmi.multitouch.drivers.diamondtouch.DiamondTouchStreamProvider;
import nl.utwente.ewi.hmi.multitouch.event.TouchEvent;
import nl.utwente.ewi.hmi.multitouch.event.TouchJoinEvent;
import nl.utwente.ewi.hmi.multitouch.event.TouchListener;
import nl.utwente.ewi.hmi.multitouch.event.TouchSplitEvent;
import nl.utwente.ewi.hmi.multitouch.io.TouchDeviceException;
import nl.utwente.ewi.hmi.multitouch.io.TouchStreamProvider;

public class DiamondTouchTest extends JFrame implements KeyListener, TouchDeviceListener {

	private Graphics2D g = null;
	
	private BufferedImage bi = null;

	private Painter painter = null;
	
//	private Dimension size = null;
	
	
	public DiamondTouchTest(TouchDevice device){
		
		this.painter = new Painter();
    	this.addKeyListener(this);
    	int width = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getWidth();
    	int height = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight();

    	bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    	this.g = (Graphics2D) bi.getGraphics();
    	g.setColor(Color.black);
    	g.fillRect(0, 0, width, height);


    	JPanel paintPanel = new JPanel() {
    		@Override
    		public void paint(Graphics g) {
    			g.drawImage( bi, 0, 0, this.getWidth(), this.getHeight(), 0, 0, bi.getWidth(), bi.getHeight(), null);
    		}
    	};
    	this.setContentPane(paintPanel);
    	this.setUndecorated(true);
//    	paintPanel.setPreferredSize(size);

    	this.pack();

    	
    	
    	device.addTouchDeviceListener(this);
    	
    	try {
			device.start();
		} catch (TouchDeviceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void touchDeviceStarted(TouchDevice touchDevice) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void touchDeviceStopped(TouchDevice touchDevice) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void touchRegistered(TouchDevice touchDevice, Touch touch) {
		painter.add(touch);
		
	}
	
	private class Painter implements TouchListener {

		Map<Touch, Color> colors = new HashMap<Touch, Color>();
		
		private Random r = new Random();
		
		void add(Touch touch) {
			if(colors.containsKey(touch)) {
				return;
			}

			int red = 128 + r.nextInt(128);
			int green = 128 + r.nextInt(128);
			int blue = 128 + r.nextInt(128);
			Color c = new Color(red, green, blue);
			this.colors.put(touch, c);
			
			draw(touch);
			touch.addTouchListener(this);
		}
		
		@Override
		public void touchJoined(TouchJoinEvent event) {
			int rSum = 0;
			int gSum = 0;
			int bSum = 0;
			
			int amount = event.getJoinedTouches().size();
			
			for(Touch touch : event.getJoinedTouches()) {
				Color c = colors.get(touch);
				if(c != null) {
					rSum += c.getRed();
					gSum += c.getGreen();
					bSum += c.getBlue();
				}
			}

			rSum /= amount;
			gSum /= amount;
			bSum /= amount;
			
			colors.put(event.getTargetTouch(), new Color(rSum, gSum, bSum));
			
			
		}

		void draw(Touch touch) {
//			int x = (int)((touch.getOrigin().getX() * getWidth()) / size.getWidth());
//			int y = (int)((touch.getOrigin().getY() * getHeight()) / size.getHeight());
			
			g.setColor(colors.get(touch));

			int x = (int)touch.getOrigin().getX();
			int y = (int)touch.getOrigin().getY();
			
			g.fill(touch.getShape());
			
			
			g.setColor(Color.red);

			g.draw(touch.getPath());
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					repaint();
					
				}
			});
			
		}
		
		@Override
		public void touchMoved(TouchEvent event) {
			draw(event.getSource());
			// TODO Auto-generated method stub
			
		}


		@Override
		public void touchReleased(TouchEvent event) {
			Color c = colors.get(event.getSource());
			colors.remove(event.getSource());
			if(c == null) return;
			
			Point2D source = event.getSource().getOriginStart();
			Point2D target = event.getSource().getOrigin();
			
			g.setColor(c);
			
			Path2D shape = new Path2D.Float();
			shape.append(event.getSource().getPath().getPathIterator(new AffineTransform(), 0.1d), false);
			g.fill(shape);
			
			g.setColor(Color.red);
			
			g.drawLine((int)source.getX(), (int)source.getY(), (int)target.getX(), (int)target.getY());
			
			repaint();
			
			
			// TODO Auto-generated method stub
			
		}

		@Override
		public void touchShapeChanged(TouchEvent event) {
			draw(event.getSource());
			// TODO Auto-generated method stub
			
		}

		@Override
		public void touchSplit(TouchSplitEvent event) {
			Color source = colors.get(event.getSource());
			for(Touch touch : event.getTargetTouches()) {
				colors.put(touch, source);
			}
		}
	}
	
	public static void main(String[] args) throws Exception {
		TouchStreamProvider provider = DiamondTouchStreamProvider.getDiamondTouchStreamProvider();
//		TouchStreamProvider provider = MouseTouchStreamProvider.getMouseTouchStreamProvider();
//		TouchStreamProvider provider = new TuioTouchStreamProvider();
		provider = new CalibratedTouchStreamProvider(provider, new FixedTransformCalibrator(new Rectangle2D.Float(0, 0, 1280f, 1024f)));;
//		provider = new CalibratedTouchStreamProvider(provider, new DefaultManualCalibrator());
		TouchEnvironment environment = TouchEnvironment.getTouchEnvironment();

		
		environment.registerTouchStreamProvider(provider);
		
		List<TouchDeviceInfo> deviceInfos = environment.getTouchDeviceInfoList();

		if(deviceInfos.isEmpty()) {
			System.exit(1);
		}
		
		TouchDeviceInfo deviceInfo = deviceInfos.get(0);
		TouchDevice device = environment.getTouchDevice(deviceInfo);
//		device.setThreaded(false);
		DiamondTouchTest app = new DiamondTouchTest(device);
		
		device.start();
		
        GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(app);
        app.setVisible(true);
		
        Object obj = new Object();
        
        synchronized(obj) {
        	obj.wait();
        }

//        while(true) {
//        	device.getTouchFrameProcessor().process();
//        	Thread.sleep(1000);
//        	device.setThreaded(Math.random() > 0.5f);
//        }
		
	}
}
