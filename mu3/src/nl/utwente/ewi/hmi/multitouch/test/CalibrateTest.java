package nl.utwente.ewi.hmi.multitouch.test;

import java.util.List;

import nl.utwente.ewi.hmi.multitouch.Touch;
import nl.utwente.ewi.hmi.multitouch.TouchDevice;
import nl.utwente.ewi.hmi.multitouch.TouchDeviceAdapter;
import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;
import nl.utwente.ewi.hmi.multitouch.TouchEnvironment;
import nl.utwente.ewi.hmi.multitouch.drivers.calibrate.CalibratedTouchStreamProvider;
import nl.utwente.ewi.hmi.multitouch.drivers.calibrate.TouchDeviceCalibrator;
import nl.utwente.ewi.hmi.multitouch.drivers.calibrate.FixedTransformCalibrator;
import nl.utwente.ewi.hmi.multitouch.drivers.diamondtouch.DiamondTouchStreamProvider;
import nl.utwente.ewi.hmi.multitouch.drivers.tuio.TuioTouchStreamProvider;
import nl.utwente.ewi.hmi.multitouch.event.TouchAdapter;
import nl.utwente.ewi.hmi.multitouch.event.TouchEvent;
import nl.utwente.ewi.hmi.multitouch.io.TouchStreamProvider;

public class CalibrateTest {

	public static void main(String[] args) throws Exception {
//		TouchStreamProvider provider = DiamondTouchStreamProvider.getDiamondTouchStreamProvider();
		TouchStreamProvider provider = new TuioTouchStreamProvider();
		
//		TouchDeviceCalibrator calibrator = new SimpleTouchDeviceCalibrator();
		TouchDeviceCalibrator calibrator = FixedTransformCalibrator.getUnitSquareFixedTransformCalibrator();
		
		CalibratedTouchStreamProvider p2 = new CalibratedTouchStreamProvider(provider, calibrator);

		TouchEnvironment environment = TouchEnvironment.getTouchEnvironment();

		
		environment.registerTouchStreamProvider(p2);
		
		List<TouchDeviceInfo> deviceInfos = environment.getTouchDeviceInfoList();
		if(deviceInfos.isEmpty()) {
			System.exit(1);
		}
		
		
		TouchDeviceInfo deviceInfo = deviceInfos.get(0);
		TouchDevice device = environment.getTouchDevice(deviceInfo);
		device.start();
		device.addTouchDeviceListener(new TouchDeviceAdapter() {

			@Override
			public void touchRegistered(TouchDevice touchDevice, Touch touch) {
				System.out.println(touch.getOrigin());
				touch.addTouchListener(new TouchAdapter() {
					@Override
					public void touchMoved(TouchEvent event) {
						System.out.println(event.getSource().getOrigin());
					}
					
					@Override
					public void touchReleased(TouchEvent event) {
						event.getSource().removeTouchListener(this);
					}
				});
			}
			
		});
		
		Object obj = new Object();
		
		synchronized(obj) {
			obj.wait();
		}
	}
}
