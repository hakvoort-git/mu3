package nl.utwente.ewi.hmi.multitouch.event;

import java.util.Set;

import nl.utwente.ewi.hmi.multitouch.Touch;

public class TouchJoinEvent extends TouchEvent {

	private Set<Touch> joinedTouches = null;
	private Touch targetTouch = null;
	
	public TouchJoinEvent(Touch touch, Set<Touch> joinedTouches, Touch targetTouch) {
		super(touch);
		this.joinedTouches = joinedTouches;
		this.targetTouch = targetTouch;
	}

	public Set<Touch> getJoinedTouches() {
		return this.joinedTouches;
	}

	public Touch getTargetTouch() {
		return this.targetTouch;
	}
}
