package nl.utwente.ewi.hmi.multitouch.event;

/**
 * An adapter for the {@link TouchListener}.
 *  
 * @author Michiel Hakvoort
 * @version 1.0
 */
public class TouchAdapter implements TouchListener {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void touchJoined(TouchJoinEvent event) {
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void touchMoved(TouchEvent event) {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void touchReleased(TouchEvent event) {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void touchShapeChanged(TouchEvent event) {
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void touchSplit(TouchSplitEvent event) {
		
	}

}
