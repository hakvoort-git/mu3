package nl.utwente.ewi.hmi.multitouch.event;

import java.util.Set;

import nl.utwente.ewi.hmi.multitouch.Touch;

public class TouchSplitEvent extends TouchEvent {

	private Touch splittedTouch = null;
	private Set<Touch> targetTouches = null;

	public TouchSplitEvent(Touch touch, Touch splittedTouch, Set<Touch> targetTouches) {
		super(touch);
		this.splittedTouch = splittedTouch;
		this.targetTouches = targetTouches;
	}

	public Touch getSplittedTouch() {
		return this.splittedTouch;
	}

	public Set<Touch> getTargetTouches() {
		return this.targetTouches;
	}
}
