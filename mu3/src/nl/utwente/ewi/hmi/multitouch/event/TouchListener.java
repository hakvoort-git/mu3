package nl.utwente.ewi.hmi.multitouch.event;

/**
 * The TouchListener is the listener interface for the {@link nl.utwente.ewi.hmi.multitouch.Touch}.
 * 
 * @author Michiel Hakvoort
 * @version 1.0
 * @see {@link nl.utwente.ewi.hmi.multitouch.Touch}
 */
public interface TouchListener {

	/**
	 * A {@link nl.utwente.ewi.hmi.multitouch.Touch} is moved.
	 * 
	 * @param event
	 */
	public void touchMoved(TouchEvent event);

	/**
	 * The shape of a {@link nl.utwente.ewi.hmi.multitouch.Touch} is changed.
	 * 
	 * @param event
	 */
	public void touchShapeChanged(TouchEvent event);

	/**
	 * A {@link nl.utwente.ewi.hmi.multitouch.Touch} is released.
	 * 
	 * @param event
	 */
	public void touchReleased(TouchEvent event);

	/**
	 * A {@link nl.utwente.ewi.hmi.multitouch.Touch} is joined with other {@link nl.utwente.ewi.hmi.multitouch.Touch Touches}. 
	 * 
	 * @param event
	 */
	public void touchJoined(TouchJoinEvent event);

	/**
	 * A {@link nl.utwente.ewi.hmi.multitouch.Touch} is split into several other {@link nl.utwente.ewi.hmi.multitouch.Touch Touches}.
	 *
	 * @param event
	 */
	public void touchSplit(TouchSplitEvent event);

}
