package nl.utwente.ewi.hmi.multitouch;

/**
 * The Tangible interface defines a type of tangible interaction with a surface.
 * A Tangible object must be able to return an actor and a surface type.
 * For multi-user environments the actor can represent any of the users
 * executing a touch. When the {@linkplain TouchDevice} is capable of detecting
 * external devices, a Tangible can represent the external device and the actor its owner.
 * 
 * @author Michiel Hakvoort
 * @version 1.0
 *
 */
public interface Tangible {

	/**
	 * Get the actor of the Tangible.
	 * 
	 * @return The actor of the Tangible
	 */
	public Object getActor();

	/**
	 * Get the {@linkplain SurfaceType} of the Tangible.
	 * 
	 * @return The {@linkplain SurfaceType} of the Tangible
	 */
	public SurfaceType getSurfaceType();

	/**
	 * Check whether a Tangible can be split into (amongst others) the target Tangible.
	 * 
	 * @param targetTangible 
	 * @return
	 */
	public boolean canSplitInto(Tangible targetTangible);
	
	/**
	 * Check whether a Tangible can be joined into the target Tangible.
	 * @param targetTangible
	 * @return True when the Tangible can be joined into the target Tangible
	 */
	public boolean canJoinInto(Tangible targetTangible);

	/**
	 * Check whether a Tangible is unique. If a Tangible is unique, at most one Segment with the
	 * corresponding Tangible type can occur in a single frame.
	 * 
	 * @return True when the Tangible is unique.
	 */
	public boolean isUnique();
}
