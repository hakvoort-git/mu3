package nl.utwente.ewi.hmi.multitouch;

/**
 * The SurfaceType is the type of surface associated with a {@linkplain Tangible}.
 * 
 * @author Michiel Hakvoort
 * @version 1.0
 */
public interface SurfaceType {

	public static final SurfaceType UNKNOWN = new SurfaceType() {
		@Override
		public boolean isStronglyConnected() {
			return true;
		}
	};

	/**
	 * A strongly connected surface will not be split into different distinct segments when the analyzer
	 * identifies a segment as constructed out of several different segments.
	 * 
	 * @return True when the surface is strongly connected.
	 */
	public boolean isStronglyConnected();
}
