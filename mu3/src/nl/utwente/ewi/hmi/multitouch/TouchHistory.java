package nl.utwente.ewi.hmi.multitouch;

import java.awt.Rectangle;
import java.util.Properties;

import com.infomatiq.jsi.rtree.RTree;

public class TouchHistory {

	private RTree tree = null;

	protected TouchHistory() {
		this.tree = new RTree();
		this.tree.init(new Properties());
	}

	public void addTouch(Touch touch) {
//		RTreeRectangle touchBounds = RTreeRectangle.getRTreeRectangle(touch.getBounds());
		
		// remove all intersecting touches (all touches containing this touch, all touches crossing this touch, all touches contained by this touch) 
		// add the relation rectangle -> int -> touch
	}

	public void getPreviousTouch(Touch touch) {
		// get the touch contained in the given bounds
	}

	private static class RTreeRectangle extends com.infomatiq.jsi.Rectangle {
		
		RTreeRectangle(int x, int y, int width, int height) {
			super(x, y, width, height);
		}
		
		RTreeRectangle(int width, int height) {
			this(0, 0, width, height);
		}

		static RTreeRectangle getRTreeRectangle(Rectangle rectangle) {
			return new RTreeRectangle(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
		}
	}
}
