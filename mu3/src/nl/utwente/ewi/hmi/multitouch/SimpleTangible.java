package nl.utwente.ewi.hmi.multitouch;

/**
 * SimpleTangible is a Tangible implementation which allows a simple
 * Tangible type combination with a given actor and SurfaceType.
 * 
 * @author Michiel Hakvoort
 * @version 1.0
 */
public class SimpleTangible extends AbstractTangible {

	private Object actor = null;
	private SurfaceType surfaceType = null;

	public SimpleTangible(Object actor, SurfaceType surfaceType) {
		this.actor = actor;
		this.surfaceType = surfaceType;
	}
	
	@Override
	public Object getActor() {
		return this.actor;
	}
	
	@Override
	public SurfaceType getSurfaceType() {
		return this.surfaceType;
	}
}

