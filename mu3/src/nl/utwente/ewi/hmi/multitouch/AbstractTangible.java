package nl.utwente.ewi.hmi.multitouch;

/**
 * AbstractTangible denotes an adapter class for the Tangible type.
 * 
 * @author Michiel Hakvoort
 *
 */
public abstract class AbstractTangible implements Tangible {

	@Override
	public boolean canJoinInto(Tangible tangible) {
		return false;
	}

	@Override
	public boolean canSplitInto(Tangible tangible) {
		return false;
	}

	@Override
	public Object getActor() {
		return null;
	}

	@Override
	public SurfaceType getSurfaceType() {
		return SurfaceType.UNKNOWN;
	}

	@Override
	public boolean isUnique() {
		return false;
	}
}
