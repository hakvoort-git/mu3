package nl.utwente.ewi.hmi.multitouch;

/**
 * An adapter for the TouchDeviceListener
 * 
 * @author Michiel Hakvoort
 * @version 1.0
 *
 */
public class TouchDeviceAdapter implements TouchDeviceListener {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void touchDeviceStarted(TouchDevice touchDevice) {
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void touchDeviceStopped(TouchDevice touchDevice) {
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void touchRegistered(TouchDevice touchDevice, Touch touch) {
		
	}

}
