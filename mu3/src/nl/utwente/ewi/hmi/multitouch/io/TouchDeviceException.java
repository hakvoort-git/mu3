package nl.utwente.ewi.hmi.multitouch.io;

import java.io.IOException;

public class TouchDeviceException extends IOException {

	private static final long serialVersionUID = 888295222971086820L;

	public TouchDeviceException(String message) {
		super(message);
	}
}
