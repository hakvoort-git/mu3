package nl.utwente.ewi.hmi.multitouch.io;

import java.net.URI;

public abstract class TouchStreamProviderLocator {

	protected static final String SCHEME_IO = "io";
	protected static final String SCHEME_TOUCH = "touch";
	
	public abstract TouchStreamProvider locateTouchStreamProvider(URI uri); 
	public abstract boolean validTouchStreamProvider(URI uri);

}
