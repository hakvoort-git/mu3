package nl.utwente.ewi.hmi.multitouch.io;

import java.util.List;

import nl.utwente.ewi.hmi.multitouch.TouchDeviceInfo;


public interface TouchStreamProvider {

	public List<? extends TouchDeviceInfo> getTouchDeviceInfoList();

	public TouchStream getTouchStream(TouchDeviceInfo deviceInfo);

}
