package nl.utwente.ewi.hmi.multitouch.io;

import java.util.Set;

import nl.utwente.ewi.hmi.multitouch.analysis.Segment;
import nl.utwente.ewi.hmi.multitouch.analysis.segmentation.FrameSegmentation;

public interface Segmentable {

	public Set<Segment> getSegments(FrameSegmentation segmentation);

}
